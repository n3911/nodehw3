const NOT_AUTHORIZED = 'Not authorized to access this resource';
const WRONG_CREDENTIALS = 'Wrong credentials';
const EMAIL_SENT = 'sent successfully';
const EMAIL_ERROR = 'email not sent';
const REGISTRATION_ERROR = 'Code 400: Registration error.';
const DUPLICATE_USERNAME = 'User with such username already exists.';
const WRONG_ROLE = 'Incorrect role. Choose the right one(shipper/driver)'
const USER_REGISTERED = 'Profile created successfully';
const SERVER_ERROR = 'Server error.';
const WRONG_PASSWORD = 'Wrong password';
const NEW_PASSWORD = 'Your new password is: ';
const RESET_PASSWORD_LETTER = 'New password sent to your email address';
const NOT_A_SHIPPER = 'Forbidden. You are not shipper.';
const NOT_A_DRIVER = 'Forbidden. You are not driver.';
const WRONG_DIMENSIONS = 'Dimensions can not be equal to zero';
const CHANGE_STATE_ERROR = "You can't change state for non existing load";
const CHANGE_LOAD = "You can't access to not yours load.";
const ERROR_OF_CHANGE = "Forbidden. You can't change these fields.";
const NO_LOADS_FOR_ID = " There are no loads with this id.";
const DELETED_TRUCK = "Truck deleted successfully.";
const ONLY_NWE_LOADS = "You can post only new loads.";
const ONLY_OWN_LOADS = " You can post only your loads.";
const NO_AVAILABLE_TRUCKS = "There are no available trucks now.";
const MISSING_TYPE = "Missing the type field.";
const TRUCK_CREATED = "Truck created successfully";
const WRONG_TRUCK_ID = "There are no trucks with this id";
const NO_TO_UPDATE_LOADING_TRUCK = "You can't update truck while on load.";
const ALREADY_ASSIGNED = "You are assigned to this car already.";
const ASSIGNED = " Truck assigned successfully.";
const RECEIVED_USR_INFO = "User info received successfully.";
const DELETED_USER_INFO= "User was successfully deleted.";
const PASSWORD_CHANGED = "Password was successfully changed";
const USER_LACKS_AUTHORITY = 'User lacks authority';
const WRONG_OLD_PASSWORD = 'Wrong old password';
const PASSWORD_TOKEN_NOT_VALID ='Password reset token is invalid or has expired.';

module.exports = {
    NOT_AUTHORIZED,
    WRONG_CREDENTIALS,
    EMAIL_SENT,
    EMAIL_ERROR,
    REGISTRATION_ERROR,
    DUPLICATE_USERNAME,
    WRONG_ROLE,
    USER_REGISTERED,
    SERVER_ERROR,
    WRONG_PASSWORD,
    NEW_PASSWORD,
    RESET_PASSWORD_LETTER,
    NOT_A_SHIPPER,
    NOT_A_DRIVER,
    WRONG_DIMENSIONS,
    CHANGE_STATE_ERROR,
    CHANGE_LOAD,
    ERROR_OF_CHANGE,
    NO_LOADS_FOR_ID,
    DELETED_TRUCK,
    ONLY_NWE_LOADS,
    ONLY_OWN_LOADS,
    NO_AVAILABLE_TRUCKS,
    MISSING_TYPE,
    TRUCK_CREATED,
    WRONG_TRUCK_ID,
    NO_TO_UPDATE_LOADING_TRUCK,
    ALREADY_ASSIGNED,
    ASSIGNED,
    RECEIVED_USR_INFO,
    DELETED_USER_INFO,
    PASSWORD_CHANGED,
    USER_LACKS_AUTHORITY,
    WRONG_OLD_PASSWORD,
    PASSWORD_TOKEN_NOT_VALID,
};
