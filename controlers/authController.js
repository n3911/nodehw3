const User = require('../models/users');
const Credentials = require('../models/credentials');
const bcrypt = require('bcryptjs');
const {validationResult} = require('express-validator');
const jwt = require('jsonwebtoken');
const {secret} = require('../config/config');
const fs = require('fs');
const generator = require('generate-password');
const sendEmail = require('../utils/sendMail');
const {
    REGISTRATION_ERROR,
    DUPLICATE_USERNAME,
    WRONG_ROLE,
    USER_REGISTERED,
    SERVER_ERROR,
    WRONG_PASSWORD,
    NEW_PASSWORD,
    RESET_PASSWORD_LETTER
} = require('../constants/responses')



const generateAccessToken = (id, role) => {
    const payload = {id, role};
    return jwt.sign(payload, secret, {expiresIn: '24h'});
}

const roles = [
    "SHIPPER",
    "DRIVER"
]

class authController {
    async registration(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                fs.appendFile('log.log', `Code 400: ${REGISTRATION_ERROR}`, () => {
                });
                return res.status(400).json({message: REGISTRATION_ERROR, errors});
            }
            const {role, email, password} = req.body;
            const candidate = await User.findOne({email});
            if (candidate) {
                fs.appendFile('logs.log', `Code 400:${DUPLICATE_USERNAME}.`, () => {
                });
                return res.status(400).json({message: DUPLICATE_USERNAME});
            }
            if (!roles.includes(role.toUpperCase())) {
                fs.appendFile('logs.log', `Code 400: ${WRONG_ROLE}\n`,
                    () => {
                    });
                return res.status(400).json({message: WRONG_ROLE});
            }
            const hashPassword = bcrypt.hashSync(password, 7);
            const credentials = await new Credentials({email: `${email}`, password: `${hashPassword}`});
            const user = await new User({
                role: `${role.toUpperCase()}`,
                email: `${email}`,
                createdDate: `${Date.now()}`
            });
            await credentials.save();
            await user.save();
            fs.appendFile('logs.log', `Code 200: ${USER_REGISTERED}\n`, () => {
            });
            return res.status(200).json({message: USER_REGISTERED});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: ${SERVER_ERROR}\n`, () => {
            });
            res.status(500).json({message: SERVER_ERROR});
        }
    }

    async login(req, res) {
        try {
            const {email, password} = req.body;
            const credentials = await Credentials.findOne({email});
            const user = await User.findOne({email});
            if (!credentials) {
                fs.appendFile('logs.log', `User ${email} was not found\n`, () => {
                });
                return res.status(400).json({message: `User ${email} was not found`});
            }
            const validPassword = bcrypt.compareSync(password, credentials.password);
            if (!validPassword) {
                fs.appendFile('logs.log', `Code 400: ${WRONG_PASSWORD}`, () => {
                });
                return res.status(400).json({message: WRONG_PASSWORD});
            }
            const token = generateAccessToken(user._id, user.role);
            fs.appendFile('logs.log', `Code 200: Login successful.\n`, () => {});
            return res.status(200).json({message: 'success', jwt_token: token});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: ${SERVER_ERROR}`, ()=>{});
            res.status(500).json({message: SERVER_ERROR});
        }
    }

    async resetPasswordLetter(req, res) {
        try {
            const user = await User.findOne({email: req.body.email});
            const credentials = await Credentials.findOne({email: req.body.email});
            if (!user)
                return res.status(400).send("user with given email doesn't exist");
            console.log(req.url);
            const link = `${req.protocol}://${req.get('host')}${req.originalUrl}/${credentials._id}`;
            await sendEmail(req.body.email, "Reset password", link);
            fs.appendFile('logs.log', `Code 200: ${RESET_PASSWORD_LETTER}\n`, () => {});
            return res.status(200).json({message: RESET_PASSWORD_LETTER});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500:  ${SERVER_ERROR}\n`, () => {});
            res.status(500).json({message:  SERVER_ERROR});
        }
    }

    async restPassword(req, res) {
        try {
            const credentials = await Credentials.findOne({id_: req.params.id});
            const newPassword = generator.generate({
                length: 15,
                numbers: true
            })
            credentials.passwrord = bcrypt.hashSync(newPassword, 7);
            await credentials.save();
            return res.status(200).json({message: `${NEW_PASSWORD}, ${newPassword}`});
        } catch (e) {
            console.log(e)
            fs.appendFile('logs.log', `Code 500: ${SERVER_ERROR}`, ()=>{});
            res.status(500).json({message: SERVER_ERROR});
        }
    }

}
module.exports = new authController();
