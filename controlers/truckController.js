const Truck = require('../models/truck');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const {
    SERVER_ERROR,
    MISSING_TYPE,
    TRUCK_CREATED,
    WRONG_TRUCK_ID,
    NO_TO_UPDATE_LOADING_TRUCK,
    ALREADY_ASSIGNED,
    ASSIGNED,
    NOT_A_SHIPPER,
    WRONG_DIMENSIONS,
    NOT_A_DRIVER,
    CHANGE_STATE_ERROR,
    CHANGE_LOAD,
    ERROR_OF_CHANGE,
    NO_LOADS_FOR_ID,
    DELETED_TRUCK,
    ONLY_NWE_LOADS,
    ONLY_OWN_LOADS,
    NO_AVAILABLE_TRUCKS
} = require('../constants/responses')

const types = ["SPRINTER", "SMALL_STRAIGHT", "LARGE_STRAIGHT"];

const status = ["OL", "IS"];

class truckController {
    async getTrucks(req, res) {
        try {
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const trucks = await Truck.find({created_by: decoded.id});
            fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
            });
            return res.status(200).json({message: "Success", trucks: trucks});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: ${SERVER_ERROR}\n`, () => {
            });
            res.status(500).json({message: SERVER_ERROR});
        }
    }

    async addTruck(req, res) {
        try {
            const {type} = req.body
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const trucks = await Truck.find({created_by: decoded.id});
            if(decoded.role !== "DRIVER") {
                fs.appendFile('logs.log', `Code 400: ${NOT_A_DRIVER}\n`, () => {
                });
                return res.status(400).json({message: NOT_A_DRIVER});
            } else if( type === undefined){
                fs.appendFile('logs.log', `Code 400: ${MISSING_TYPE}\n`, () => {
                });
                return res.status(400).json({message: MISSING_TYPE});
            } else if (!type.includes(type.toUpperCase())){
                fs.appendFile('logs.log', `Code 400: Wrong type\n`, () => {
                });
                return res.status(400).json({message: "Wrong type"});
            }
            const truck = await new Truck({
                created_by: decoded.id,
                type: type.toUpperCase(),
                status: status[1],
                created_date: Date.now()
            });
            await truck.save();
            fs.appendFile('logs.log', `Code 200: ${TRUCK_CREATED}\n`, () => {
            });
            return res.status(200).json({message:TRUCK_CREATED});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: ${SERVER_ERROR}\n`, () => {
            });
            res.status(500).json({message: SERVER_ERROR});
        }
    }

    async getTruckById(req, res) {
        try {
            const {id} = req.params;
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "DRIVER") {
                fs.appendFile('logs.log', `Code 400: ${NOT_A_DRIVER}\n`, () => {
                });
                return res.status(400).json({message: NOT_A_DRIVER});
            }
            const truck = await Truck.findOne({_id: id});
            fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
            });
            return res.status(200).json({message: "Success", truck: truck});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: ${SERVER_ERROR}\n`, () => {
            });
            res.status(500).json({message: SERVER_ERROR});
        }
    }

    async updateTruck(req, res) {
        try {
            const {id} = req.params;
            const {type} = req.body;
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "DRIVER") {
                fs.appendFile('logs.log', `Code 400: ${NOT_A_DRIVER}\n`, () => {
                });
                return res.status(400).json({message: NOT_A_DRIVER});
            } else if( type === undefined) {
                fs.appendFile('logs.log', `Code 400: ${MISSING_TYPE}\n`, () => {
                });
                return res.status(400).json({message: MISSING_TYPE});
            }
            const truck = await Truck.findOne({_id: id});
            if (!types.includes(type.toUpperCase()) || type.toUpperCase() === truck.type) {
                fs.appendFile('logs.log', `Code 400: Wrong type\n`, () => {
                });
                return res.status(400).json({message: "Wrong type"});
            } else if (truck === null) {
                fs.appendFile('logs.log', `Code 400: ${WRONG_TRUCK_ID}\n`, () => {
                });
                return res.status(400).json({message: WRONG_TRUCK_ID});
            } else if (truck.status === "OL") {
                fs.appendFile('logs.log', `Code 400: ${NO_TO_UPDATE_LOADING_TRUCK}\n`, () => {
                });
                return res.status(400).json({message: NO_TO_UPDATE_LOADING_TRUCK});
            }
            truck.type = type.toUpperCase();
            await truck.save();
            fs.appendFile('logs.log', `Code 200: Success.\n`, () => {
            });
            return res.status(200).json({message: "Success", trucks: truck});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: ${SERVER_ERROR}\n`, () => {
            });
            res.status(500).json({message: SERVER_ERROR});
        }
    }

    async deleteTruck(req, res) {
        try {
            const {id} = req.params;
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "DRIVER") {
                fs.appendFile('logs.log', `Code 400: ${NOT_A_DRIVER}\n`, () => {
                });
                return res.status(400).json({message: NOT_A_DRIVER});
            }
            const truck = await Truck.findOne({_id: id});
            if (truck === null) {
                fs.appendFile('logs.log', `Code 400: ${NO_LOADS_FOR_ID}\n`, () => {
                });
                return res.status(400).json({message:NO_LOADS_FOR_ID});
            } else if (truck.status === "OL") {
                fs.appendFile('logs.log', `Code 400: ${ NO_TO_UPDATE_LOADING_TRUCK }\n`, () => {
                });
                return res.status(400).json({message:  NO_TO_UPDATE_LOADING_TRUCK});
            }
            truck.deleteOne();
            fs.appendFile('logs.log', `Code 200: ${DELETED_TRUCK}\n`, () => {
            });
            return res.status(200).json({message: DELETED_TRUCK});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: ${SERVER_ERROR}\n`, () => {
            });
            res.status(500).json({message: SERVER_ERROR});
        }
    }

    async assignTruck(req, res) {
        try {
            const {id} = req.params;
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            if (decoded.role !== "DRIVER") {
                fs.appendFile('logs.log', `Code 400: ${NOT_A_DRIVER}\n`, () => {
                });
                return res.status(400).json({message: NOT_A_DRIVER});
            }
            const oldTruck = await Truck.findOne({assigned_to: decoded.id})
            const truck = await Truck.findOne({_id: id});
            if (truck === null) {
                fs.appendFile('logs.log', `Code 400: ${NO_LOADS_FOR_ID}\n`, () => {
                });
                return res.status(400).json({message: NO_LOADS_FOR_ID});
            } else if (truck.status === "OL") {
                fs.appendFile('logs.log', `Code 400: ${NO_TO_UPDATE_LOADING_TRUCK}}\n`, () => {
                });
                return res.status(400).json({message: NO_TO_UPDATE_LOADING_TRUCK});
            } else if (oldTruck._id.toString() === id) {
                fs.appendFile('logs.log', `Code 400: ${ALREADY_ASSIGNED}\n`, () => {
                });
                return res.status(400).json({message: ALREADY_ASSIGNED});
            }
            truck.assigned_to = decoded.id;
            oldTruck.assigned_to = undefined;
            await truck.save();
            await oldTruck.save();
            fs.appendFile('logs.log', `Code 200: ${ASSIGNED}\n`, () => {
            });
            return res.status(200).json({message: ASSIGNED});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: ${SERVER_ERROR}\n`, () => {
            });
            res.status(500).json({message: SERVER_ERROR});
        }
    }
}


module.exports = new truckController();
