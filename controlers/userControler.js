const User = require('../models/users');
const Credentials = require('../models/credentials');
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const fs = require("fs");
const {
    RECEIVED_USR_INFO,
    SERVER_ERROR,
    DELETED_USER_INFO,
    PASSWORD_CHANGED
} = require('../constants/responses')


class userController {
    async getUser(req, res) {
        try {
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const user = await User.findById(decoded.id);
            fs.appendFile('logs.log', `Code 200: ${RECEIVED_USR_INFO}.\n`, () => {
            });
            return res.status(200).json(user);
        } catch (e) {
            console.log(e)
            fs.appendFile('logs.log', `Code 500: ${SERVER_ERROR}\n`, () => {
            });
            res.status(500).json({message: SERVER_ERROR});
        }
    }

    async deleteUser(req, res) {
        try {
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const user = await User.findById(decoded.id);
            await Credentials.find({email: user.email}).deleteOne();
            await user.deleteOne();
            fs.appendFile('logs.log', `Code 200: ${DELETED_USER_INFO}\n`, () => {
            });
            return res.status(200).json({message: DELETED_USER_INFO});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: ${SERVER_ERROR}\n`, () => {
            });
            res.status(500).json({message: SERVER_ERROR});
        }
    }

    async updateUser(req, res) {
        try {
            const {oldPassword, newPassword} = req.body;
            let token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const user = await User.findById(decoded.id);
            const cred = await Credentials.findOne({email: user.email});
            const validPassword = bcrypt.compareSync(oldPassword, cred.password);
            if (!validPassword) {
                fs.appendFile('logs.log', `Code 400: Wrong password.\n`, () => {
                });
                return res.status(400).json({message: "Wrong password"});
            }
            cred.password = bcrypt.hashSync(newPassword, 10);
            await cred.save();
            fs.appendFile('logs.log', `Code 200: ${PASSWORD_CHANGED}\n`, () => {
            });
            return res.status(200).json({message: PASSWORD_CHANGED, jwt_token: token});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: ${SERVER_ERROR}\n`, () => {
            });
            res.status(500).json({message: SERVER_ERROR});
        }
    }
}

module.exports = new userController();
