const express = require('express');
const mongoose = require('mongoose');
const authRouter = require('./routers/authRouter');
const truckRouter = require('./routers/truckRouter')
const userRouter = require('./routers/userRouter');
const loadRouter = require('./routers/loadRouter');



require('dotenv').config();
const PORT = process.env.PORT || 8080;
const app = express();

app.use(express.json());
app.use("/api/auth", authRouter);
app.use("/api/users", userRouter);
app.use("/api/loads", loadRouter);
app.use("/api/trucks", truckRouter);


const start = async () => {
    try {
        await mongoose.connect(process.env.MONGO_URI);
        app.listen(PORT, () => console.log(`server has been started on port ${PORT}`))
    } catch (e) {
        console.log(e)
    }
}

start();
