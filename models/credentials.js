const {Schema, model} = require('mongoose');

const Credentials = new Schema({
    email: {type: String, unique: true, required: true},
    password: {type: String, unique: false, required: true}
})

module.exports = model('Credentials', Credentials);
