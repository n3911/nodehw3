const {Schema, model} = require('mongoose');

const Load = new Schema({
    created_by: {type: String, unique: false, required: true},
    assigned_to: {type: String, unique: false, required: false},
    status: {type: String, unique: false, required: true},
    state: {type: String, unique: false, required: false},
    name: {type: String, unique: false, required: true},
    payload: {type: Number, unique: false, required: true},
    pickup_address: {type: String, unique: false, required: true},
    delivery_address: {type: String, unique: false, required: true},
    dimensions: {type: {}, unique: false, required: true},
    logs: {
        type: [{
            message: {type: String, unique: false, required: true},
            time: {type: Date, unique: false, required: true}
        }], unique: false, required: true
    },
    created_date: {type: Date, unique: false, required: true}
})

module.exports = model('Load', Load);
