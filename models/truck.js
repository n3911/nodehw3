const {Schema, model} = require('mongoose');

const Truck = new Schema({
    created_by: {type: String, unique: false, required: true},
    assigned_to: {type: String, unique: false, required: false},
    type: {type: String, unique: false, required: true},
    status: {type: String, unique: false, required: true},
    created_date: {type: Date, unique: false, required: true}
})

module.exports = model('Truck', Truck);
