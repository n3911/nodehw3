const { Schema, model} = require('mongoose');

const User = new Schema({
    role: {type: String, unique: false, required: true},
    email: {type: String, unique: true, required: true},
    createdDate: {type: Date, unique: false, required: true}
})
module.exports = model('User', User);
