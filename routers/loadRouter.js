const Router = require('express');
const router = new Router();
const controller = require('../controlers/loadController');
const joi = require('joi');
const authMiddleware = require("../middlewares/authMiddleware");
const validator = require('express-joi-validation').createValidator({});


const querySchema = joi.object({
    name: joi.string().min(1),
    payload: joi.number().min(1),
    pickup_address: joi.string().min(1),
    delivery_address: joi.string().min(1),
})

router.get('/', authMiddleware, controller.getLoads);
router.post('/', [
    authMiddleware,
    validator.query(querySchema)
], controller.addLoad);
router.get('/active', authMiddleware, controller.getActiveLoad);
router.patch('/active/state', authMiddleware, controller.changeState);
router.get('/:id', authMiddleware, controller.getUserLoads);
router.put('/:id', [
    authMiddleware,
    validator.query(querySchema)
], controller.updateLoad);
router.delete('/:id', authMiddleware, controller.deleteLoad);
router.post('/:id/post', authMiddleware, controller.postLoad);
router.get('/:id/shipping_info', authMiddleware, controller.getShippingInfo);


module.exports = router;
