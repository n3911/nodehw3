const Router = require('express');
const router = new Router();
const controller = require('../controlers/truckController');
const authMiddleware = require("../middlewares/authMiddleware");


router.get('/', authMiddleware, controller.getTrucks);
router.post('/', authMiddleware, controller.addTruck);
router.get('/:id', authMiddleware, controller.getTruckById);
router.put('/:id', authMiddleware, controller.updateTruck);
router.delete('/:id', authMiddleware, controller.deleteTruck);
router.post('/:id/assign', authMiddleware, controller.assignTruck);


module.exports = router;
