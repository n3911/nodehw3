const Router = require('express');
const router = new Router();
const controller = require('../controlers/userControler')
const authMiddleware = require('../middlewares/authMiddleware');
const joi = require('joi');
const validator = require('express-joi-validation').createValidator({})

const querySchema = joi.object({
    oldPassword: joi.string().min(4).max(16),
    newPassword: joi.string().min(4).max(16)
})

router.get("/me",authMiddleware, controller.getUser);
router.delete("/me",authMiddleware, controller.deleteUser);
router.patch("/me",[
    authMiddleware,
    validator.query(querySchema)
], controller.updateUser);


module.exports = router;
