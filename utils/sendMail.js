const nodemailer = require('nodemailer');
const {EMAIL_SENT, EMAIL_ERROR } = require('../constants/responses')

const sendEmail = async (email, subject, text) => {
    try {
        let testAccount = await nodemailer.createTransport();

        const transporter = nodemailer.createTransport({
            host: 'oleksiHW3.gmail.com',
            port:450,
            secure: false,
            auth: {
                user: process.env.USER,
                pass: process.env.PASS
            },
        })
        await transporter.sendMail({
            from: process.env.USER,
            to: email,
            subject: subject,
            text: text,
        })
        console.log(EMAIL_SENT)
    } catch (err) {
        console.log(err, EMAIL_ERROR)
    }
}
module.exports = sendEmail;
